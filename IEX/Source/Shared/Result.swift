//
//  Result.swift
//  IEX
//
//  Created by Robert Colin on 6/23/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

public enum Result<T> {
    case success(T)
    case failure(Error)
}
