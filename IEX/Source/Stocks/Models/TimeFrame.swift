//
//  TimeFrame.swift
//  IEX
//
//  Created by Robert Colin on 11/27/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

public enum TimeFrame: String {
    case fiveYear = "5y"
    case twoYear = "2y"
    case year = "1y"
    case yearToDate = "ytd"
    case sixMonths = "6m"
    case threeMonths = "3m"
    case month = "1m"
    case day = "1d"
    case dynamic
}
