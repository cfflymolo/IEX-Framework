//
//  ChartRequest.swift
//  IEX
//
//  Created by Robert Colin on 9/4/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

internal struct ChartRequest: APIRequest {
    internal var path: String { return "/1.0/stock/\(symbol)/chart/\(timeFrame)" }
    internal let method: HTTPMethod = .get
    
    internal var bodyData: Data? { return nil }
    
    internal let symbol: String
    internal let timeFrame: TimeFrame
    
    internal func headers() -> [String : String]? {
        return [
            "Accept": "application/json"
        ]
    }
    
    internal func queryParameters() -> [String : String]? { return nil }
    internal func parameters() throws -> [String : Any] { return [:] }
}
