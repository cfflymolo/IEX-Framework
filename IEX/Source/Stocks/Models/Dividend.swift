//
//  Dividend.swift
//  IEX
//
//  Created by Robert Colin on 2/26/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

public struct Dividend: Decodable {
    public let amount: Float
    public let declaredDate: Date
    public let exDividendDate: Date
    public let flag: String
    public let paymentDate: Date
    public let qualified: String
    public let recordDate: Date
    public let type: String
    
    enum CodingKeys: String, CodingKey {
        case amount
        case declaredDate
        case exDividendDate = "exDate"
        case flag
        case paymentDate
        case qualified
        case recordDate
        case type
    }
}

extension Dividend {
    public init(from decoder: Decoder) throws {
        func dateForKey<Key>(fromContainer container: KeyedDecodingContainer<Key>, key: KeyedDecodingContainer<Key>.Key) throws -> Date {
            let dateString = try container.decode(String.self, forKey: key)
            
            if let date = DateFormatter.iso8601Basic.date(from: dateString) {
                return date
            } else {
                throw DecodingError.dataCorruptedError(
                    forKey: key,
                    in: container,
                    debugDescription: "Date string does not match the format expected by formatter."
                )
            }
        }
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        amount = try container.decode(Float.self, forKey: .amount)
        flag = try container.decode(String.self, forKey: .flag)
        qualified = try container.decode(String.self, forKey: .qualified)
        type = try container.decode(String.self, forKey: .type)
        
        declaredDate = try dateForKey(fromContainer: container, key: .declaredDate)
        exDividendDate = try dateForKey(fromContainer: container, key: .exDividendDate)
        paymentDate = try dateForKey(fromContainer: container, key: .paymentDate)
        recordDate = try dateForKey(fromContainer: container, key: .recordDate)
    }
}
