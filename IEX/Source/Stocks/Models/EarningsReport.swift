//
//  EarningsReport.swift
//  IEX
//
//  Created by Robert Colin on 2/26/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

public struct EarningsReport: Decodable {
    let symbol: String
    let earnings: [Earning]
}

public struct Earning: Decodable {
    public let announceTime: String
    public let actualEarningsPerShare: Float
    public let consensusEarningsPerShare: Float
    public let estimatedEarningsPerShare: Float
    public let fiscalEndDate: Date
    public let fiscalPeriod: String
    public let numberOfEstimates: Int
    public let reportDate: Date
    public let surpriseAmount: Float
    
    enum CodingKeys: String, CodingKey {
        case announceTime
        case actualEarningsPerShare = "actualEPS"
        case consensusEarningsPerShare = "consensusEPS"
        case estimatedEarningsPerShare = "estimatedEPS"
        case fiscalEndDate
        case fiscalPeriod
        case reportDate = "EPSReportDate"
        case surpriseAmount = "EPSSurpriseDollar"
        case numberOfEstimates
    }
}

extension Earning {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        actualEarningsPerShare = try container.decode(Float.self, forKey: .actualEarningsPerShare)
        consensusEarningsPerShare = try container.decode(Float.self, forKey: .consensusEarningsPerShare)
        estimatedEarningsPerShare = try container.decode(Float.self, forKey: .estimatedEarningsPerShare)
        announceTime = try container.decode(String.self, forKey: .announceTime)
        fiscalPeriod = try container.decode(String.self, forKey: .fiscalPeriod)
        numberOfEstimates = try container.decode(Int.self, forKey: .numberOfEstimates)
        surpriseAmount = try container.decode(Float.self, forKey: .surpriseAmount)
        
        let fiscalEndDateString = try container.decode(String.self, forKey: .fiscalEndDate)
        if let fiscalDate = DateFormatter.iso8601Basic.date(from: fiscalEndDateString) {
            self.fiscalEndDate = fiscalDate
        } else {
            throw DecodingError.dataCorruptedError(
                forKey: CodingKeys.fiscalEndDate,
                in: container,
                debugDescription: "Date string does not match format expected by formatter."
            )
        }
        
        let reportDateString = try container.decode(String.self, forKey: .reportDate)
        if let reportDate = DateFormatter.iso8601Basic.date(from: reportDateString) {
            self.reportDate = reportDate
        } else {
            throw DecodingError.dataCorruptedError(
                forKey: CodingKeys.reportDate,
                in: container,
                debugDescription: "Date string does not match format expected by formatter."
            )
        }
    }
}
