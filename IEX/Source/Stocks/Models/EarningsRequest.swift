//
//  EarningsRequest.swift
//  IEX
//
//  Created by Robert Colin on 6/28/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

internal struct EarningsRequest: APIRequest {
    internal var path: String { return "/1.0/stock/\(symbol)/earnings" }
    internal let method: HTTPMethod = .get
    
    internal var bodyData: Data? { return nil }
    
    internal let symbol: String
    
    internal func headers() -> [String : String]? { return ["Accept": "application/json"] }
    internal func queryParameters() -> [String : String]? { return nil }
    internal func parameters() throws -> [String : Any] { return [:] }
}
