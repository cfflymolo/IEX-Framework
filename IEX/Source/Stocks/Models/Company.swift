//
//  Company.swift
//  IEX
//
//  Created by Robert Colin on 2/26/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

public struct Company: Decodable {
    public let symbol: String
    public let name: String
    public let description: String
    public let exchange: String
    public let industry: String
    public let ceo: String
    
    enum CodingKeys: String, CodingKey {
        case symbol
        case name = "companyName"
        case description
        case exchange
        case industry
        case ceo = "CEO"
    }
}
