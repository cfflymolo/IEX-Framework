//
//  News.swift
//  IEX
//
//  Created by Robert Colin on 2/26/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

public struct News: Decodable {
    public let date: Date
    public let headline: String
    public let source: String
    public let url: URL
    public let summary: String
    
    enum CodingKeys: String, CodingKey {
        case date = "datetime"
        case headline
        case source
        case url
        case summary
    }
}

extension News {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        headline = try container.decode(String.self, forKey: .headline)
        source = try container.decode(String.self, forKey: .source)
        summary = try container.decode(String.self, forKey: .summary)
        url = try container.decode(URL.self, forKey: .url)
        
        let dateString = try container.decode(String.self, forKey: .date)
        if let date = DateFormatter.iso8601Full.date(from: dateString) {
            self.date = date
        } else {
            throw DecodingError.dataCorruptedError(
                forKey: CodingKeys.date,
                in: container,
                debugDescription: "Date string does not match format expected by formatter."
            )
        }
    }
}
