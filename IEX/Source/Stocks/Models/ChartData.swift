//
//  ChartData.swift
//  IEX
//
//  Created by Robert Colin on 10/18/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

public struct ChartData: Decodable {
    public let date: Date
    
    public let minute: String?
    public let label: String
    
    public let high: Float
    public let low: Float
    public let volume: Int?
    public let notional: Float?
    public let numberOfTrades: Int
    
    public let open: Float
    public let close: Float
    public let marketOpen: Float?
    public let marketClose: Float?
    
    public let change: Float
    public let changeOverTime: Float
    public let changePercent: Float
}
