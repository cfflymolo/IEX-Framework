//
//  DelayedQuote.swift
//  IEX
//
//  Created by Robert Colin on 2/26/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

public struct DelayedQuote: Decodable {
    public let delayedPrice: Float
    public let delayedSize: Int
    public let delayedPriceTime: Int
    public let high: Float
    public let low: Float
    public let processedTime: Int
    public let symbol: String
}
