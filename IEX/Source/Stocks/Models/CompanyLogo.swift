//
//  CompanyLogo.swift
//  IEX
//
//  Created by Robert Colin on 6/27/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

public struct CompanyLogo: Codable {
    public let url: URL
}
