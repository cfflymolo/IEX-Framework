//
//  CompanyController.swift
//  IEX
//
//  Created by Robert Colin on 3/4/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

public class CompanyController {
    public typealias CompanyInfoResult = ((Result<Company>) -> ())
    public typealias CompanyLogoResult = ((Result<CompanyLogo>) -> ())
    public typealias CompanyNewsResult = ((Result<[News]>) -> ())
    public typealias DelayedQuoteResult = ((Result<DelayedQuote>) -> ())
    public typealias DividendsResult = ((Result<[Dividend]>) -> ())
    public typealias EarningsResult = ((Result<EarningsReport>) -> ())
    
    let endpointController: EndpointController
    
    public init() {
        endpointController = EndpointController(hostUrlString: "api.iextrading.com")
    }
    
    // MARK - Public
    
    public func companyInfo(forSymbol symbol: String, completion: @escaping CompanyInfoResult) {
        let companyInfoRequest: CompanyInfoRequest = CompanyInfoRequest(symbol: symbol)
        
        endpointController.execute(companyInfoRequest, completion: completion)
    }
    
    public func chartData(
        forSymbol symbol: String,
        timeFrame: TimeFrame,
        completion: @escaping (Result<[ChartData]>) -> ()
    )
    {
        let chartRequest = ChartRequest(symbol: symbol, timeFrame: timeFrame)
        
        endpointController.execute(chartRequest, completion: completion)
    }
    
    public func companyLogo(forSymbol symbol: String, completion: @escaping CompanyLogoResult) {
        let companyLogoRequest: CompanyLogoRequest = CompanyLogoRequest(symbol: symbol)
        
        endpointController.execute(companyLogoRequest, completion: completion)
    }
    
    public func companyNews(forSymbol symbol: String, completion: @escaping CompanyNewsResult) {
        let companyNewsRequest: CompanyNewsRequest = CompanyNewsRequest(symbol: symbol)
        
        endpointController.execute(companyNewsRequest, completion: completion)
    }
    
    public func delayedQuote(forSymbol symbol: String, completion: @escaping DelayedQuoteResult) {
        let delayedQuoteRequest: DelayedQuoteRequest = DelayedQuoteRequest(symbol: symbol)
        
        endpointController.execute(delayedQuoteRequest, completion: completion)
    }
    
    public func dividends(
        forSymbol symbol: String,
        timeFrame: TimeFrame,
        completion: @escaping DividendsResult
    )
    {
        let dividendRequest: DividendRequest = DividendRequest(symbol: symbol, timeFrame: timeFrame)
        
        endpointController.execute(dividendRequest, completion: completion)
    }
    
    public func earningsReport(forSymbol symbol: String, completion: @escaping EarningsResult) {
        let earningsRequest: EarningsRequest = EarningsRequest(symbol: symbol)
        
        endpointController.execute(earningsRequest, completion: completion)
    }
}
