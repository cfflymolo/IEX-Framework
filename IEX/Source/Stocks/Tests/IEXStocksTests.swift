//
//  IEXStocksTests.swift
//  IEXTests
//
//  Created by Robert Colin on 3/7/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import XCTest

@testable import IEX

class IEXStocksTests: XCTestCase {
    
    var controllerUnderTest: CompanyController?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        controllerUnderTest = CompanyController()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        controllerUnderTest = nil
    }
    
    func testSuccessfulCompanyInfoResponse() {
        let promise: XCTestExpectation = expectation(
            description: "Company info response from the endpoint."
        )
        
        var requestError: Error?
        var companyResponse: Company?
        
        controllerUnderTest?.companyInfo(forSymbol: "AAPL") { result in
            switch result {
            case .failure(let error): requestError = error
            case .success(let response): companyResponse = response
            }
            
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertNotNil(companyResponse)
        XCTAssertNil(requestError)
    }
    
    func testSuccessfulCompanyLogoResponse() {
        let promise: XCTestExpectation = expectation(
            description: "Company logo resposne from the endpoint"
        )
        
        var requestError: Error?
        var companyLogo: CompanyLogo?
        
        controllerUnderTest?.companyLogo(forSymbol: "AAPL") { result in
            switch result {
            case .failure(let error): requestError = error
            case .success(let result): companyLogo = result
            }
            
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertNotNil(companyLogo)
        XCTAssertNil(requestError)
    }
    
    func testSuccessfulChartResponse() {
        let promise: XCTestExpectation = expectation(
            description: "Successful chart resposne from the endpoint."
        )
        
        var chartError: Error?
        var chartResponse: [ChartData]?
        
        controllerUnderTest?.chartData(forSymbol: "AAPL", timeFrame: .day) { result in
            switch result {
            case .failure(let error): chartError = error
            case .success(let response): chartResponse = response
            }
            
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertNil(chartError)
        XCTAssertNotNil(chartResponse)
    }
    
    func testSuccessfulCompanyNewsResponse() {
        let promise: XCTestExpectation = expectation(
            description: "Company news response from the endpoint."
        )
        
        var requestError: Error?
        var newsResponse: [News]?
        
        controllerUnderTest?.companyNews(forSymbol: "AAPL") { result in
            switch result {
            case .failure(let error): requestError = error
            case .success(let response): newsResponse = response
            }
            
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertNotNil(newsResponse)
        XCTAssertNil(requestError)
    }
    
    func testSuccessfulDelayedQuoteResponse() {
        let promise: XCTestExpectation = expectation(
            description: "Delayed quote response from the endpoint."
        )
        
        var requestError: Error?
        var delayedQuoteResponse: DelayedQuote?
        
        controllerUnderTest?.delayedQuote(forSymbol: "AAPL") { result in
            switch result {
            case .failure(let error): requestError = error
            case .success(let result): delayedQuoteResponse = result
            }
            
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertNotNil(delayedQuoteResponse)
        XCTAssertNil(requestError)
    }
    
    func testSuccessfulDividendsResposne() {
        let promise: XCTestExpectation = expectation(
            description: "Dividend response from the endpoint."
        )
        
        var requestError: Error?
        var dividendResponse: [Dividend]?
        
        controllerUnderTest?.dividends(forSymbol: "AAPL", timeFrame: .yearToDate) { result in
            switch result {
            case .failure(let error): requestError = error
            case .success(let response): dividendResponse = response
            }
            
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertNotNil(dividendResponse)
        XCTAssertNil(requestError)
    }
    
    func testSuccessfulEarningsReportResponse() {
        let promise: XCTestExpectation = expectation(
            description: "Earnings report resposne from the endpoint."
        )
        
        var errorResponse: Error?
        var earningsResponse: EarningsReport?
        
        controllerUnderTest?.earningsReport(forSymbol: "AAPL") { result in
            switch result {
            case .failure(let error): errorResponse = error
            case .success(let response): earningsResponse = response
            }
            
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertNotNil(earningsResponse)
        XCTAssertNil(errorResponse)
    }
}
