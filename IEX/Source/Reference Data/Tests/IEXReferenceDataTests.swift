//
//  IEXReferenceDataTests.swift
//  IEXReferenceDataTests
//
//  Created by Robert Colin on 3/14/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import XCTest

class IEXReferenceDataTests: XCTestCase {
    
    var controllerUnderTest: ReferenceDataController?
    
    override func setUp() {
        super.setUp()
        controllerUnderTest = ReferenceDataController()
    }
    
    override func tearDown() {
        controllerUnderTest = nil
        
        super.tearDown()
    }
    
    func testSuccessfulSymbolsResponse() {
        let promise: XCTestExpectation = expectation(
            description: "Symbol data response from the endpoint."
        )
        
        var errorResponse: Error?
        var symbolResponse: [Symbol]?
        
        controllerUnderTest?.fetchSymbolData { result in
            switch result {
            case .success(let symbols):
                symbolResponse = symbols
            case .failure(let error):
                errorResponse = error
            }
            
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: nil)
        
        XCTAssertNil(errorResponse)
        XCTAssertNotNil(symbolResponse)
    }
    
    func testSymbolProcessing() {
        let symbolString: String = """
            [
                {
                    "symbol": "A",
                    "name": "Agilent Technologies Inc.",
                    "date": "2018-06-29",
                    "isEnabled": true,
                    "type": "cs",
                    "iexId": "2"
                },
                {
                    "symbol": "AA",
                    "name": "Alcoa Corporation",
                    "date": "2018-06-29",
                    "isEnabled": true,
                    "type": "cs",
                    "iexId": "12042"
                },
                {
                    "symbol": "AABA",
                    "name": "Altaba Inc.",
                    "date": "2018-06-29",
                    "isEnabled": true,
                    "type": "cs",
                    "iexId": "7653"
                },
                {
                    "symbol": "XLMUSDT",
                    "name": "Stellar Lumens USD",
                    "date": "2018-06-29",
                    "isEnabled": true,
                    "type": "crypto",
                    "iexId": 10000016
                },
                {
                    "symbol": "QTUMUSDT",
                    "name": "Qtum USD",
                    "date": "2018-06-29",
                    "isEnabled": true,
                    "type": "crypto",
                    "iexId": 10000017
                }
            ]
        """
        
        guard let symbolData: Data = symbolString.data(using: .utf8) else {
            XCTFail("Could not convert string into data.")
            return
        }
        
        let symbols: [Symbol] = try! JSONDecoder().decode([Symbol].self, from: symbolData)
        
        XCTAssertNotNil(symbols)
        XCTAssertTrue(symbols.count == 5)
    }
}
