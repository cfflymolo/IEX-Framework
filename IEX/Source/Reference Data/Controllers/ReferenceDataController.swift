//
//  ReferenceDataController.swift
//  IEX
//
//  Created by Robert Colin on 3/14/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

public final class ReferenceDataController {
    public typealias SymbolsResult = ((Result<[Symbol]>) -> ())
    
    // MARK: - Instantiation
    
    let endpointController: EndpointController
    
    // MARK: - Instantiation
    
    public init() {
        endpointController = EndpointController(hostUrlString: "api.iextrading.com")
    }
    
    // MARK: - Public
    
    public func fetchSymbolData(completion: @escaping SymbolsResult) {
        let symbolsRequest: SymbolsRequest = SymbolsRequest()
        
        endpointController.execute(symbolsRequest, completion: completion)
    }
}
