//
//  SymbolsRequest.swift
//  IEX
//
//  Created by Robert Colin on 6/24/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

internal struct SymbolsRequest: APIRequest {
    internal let path: String = "/1.0/ref-data/symbols"
    internal let method: HTTPMethod = .get
    
    internal var bodyData: Data? { return nil }
    
    internal func headers() -> [String : String]? { return nil }
    internal func queryParameters() -> [String : String]? { return nil }
    internal func parameters() throws -> [String : Any] { return [:] }
}
