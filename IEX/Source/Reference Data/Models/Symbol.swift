//
//  Symbol.swift
//  IEX
//
//  Created by Robert Colin on 3/14/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

public enum IssueType: String, Codable {
    case americanDepositoryReceipt = "ad"
    case bo
    case closedEndFund = "ce"
    case commonStock = "cs"
    case cryptoCurrency = "crypto"
    case exchangeTradedFund = "et"
    case limitedPartnerships = "lp"
    case notAvailable = "N/A"
    case preferredStock = "ps"
    case realEstateInvestmentTrust = "re"
    case secondaryIssue = "si"
    case securityUnits = "su"
}

public struct Symbol: Decodable {
    public let identifier: Int
    
    public let dateGenerated: Date
    public let isEnabled: Bool
    public let name: String
    public let symbol: String
    public let type: IssueType
    
    enum CodingKeys: String, CodingKey {
        case identifier = "iexId"
        case dateGenerated = "date"
        case isEnabled
        case name
        case symbol
        case type
    }
}

extension Symbol {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if
            let identifierString = try? container.decode(String.self, forKey: .identifier),
            let identifierId = Int(identifierString)
        {
            identifier = identifierId
        }
        else if let identifierId = try? container.decode(Int.self, forKey: .identifier)
        {
            identifier = identifierId
        }
        else
        {
            throw DecodingError.dataCorruptedError(
                forKey: .identifier,
                in: container,
                debugDescription: "Identifier type does not match the formats expected."
            )
        }
        
        isEnabled = try container.decode(Bool.self, forKey: .isEnabled)
        name = try container.decode(String.self, forKey: .name)
        symbol = try container.decode(String.self, forKey: .symbol)
        type = try container.decode(IssueType.self, forKey: .type)
        
        let dateString: String = try container.decode(String.self, forKey: .dateGenerated)
        if let date = DateFormatter.iso8601Basic.date(from: dateString) {
            dateGenerated = date
        } else {
            throw DecodingError.dataCorruptedError(
                forKey: .dateGenerated,
                in: container,
                debugDescription: "Date string does not match the format expected by the formatter."
            )
        }
    }
}
