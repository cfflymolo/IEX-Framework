//
//  Cryptocurrency.swift
//  IEX
//
//  Created by Robert Colin on 11/27/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

public struct Cryptocurrency: Decodable {
    public let symbol: String
    
    public let companyName: String
    public let primaryExchange: String
    public let sector: String
    
    public let open: Float
    public let openTime: TimeInterval
    public let close: Float
    public let closeTime: TimeInterval
    
    public let high: Float
    public let low: Float
    public let latestPrice: Float
    
    public let latestSource: String
    public let latestTime: String
    public let latestUpdate: TimeInterval
    public let latestVolume: Float
}
