//
//  AllCryptoRequest.swift
//  IEX
//
//  Created by Robert Colin on 11/27/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

internal struct AllCryptoRequest: APIRequest {
    internal let path: String = "/stock/market/crypto"
    internal let method: HTTPMethod = .get
    
    internal var bodyData: Data? { return nil }
    
    internal func headers() -> [String : String]? { return ["Accept": "application/json"] }
    internal func queryParameters() -> [String : String]? { return nil }
    internal func parameters() throws -> [String : Any] { return [:] }
}
