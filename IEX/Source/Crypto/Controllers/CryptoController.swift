//
//  CryptoController.swift
//  IEX
//
//  Created by Robert Colin on 11/27/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

public class CryptoController {
    let endpointController: EndpointController
    
    // MARK: - Initialization
    
    public init() {
        endpointController = EndpointController(hostUrlString: "api.iextrading.com")
    }
    
    // MARK: - Public
    
    public func allCrypto(completion: @escaping (Result<[Cryptocurrency]>) -> ()) {
        let cryptoRequest = AllCryptoRequest()
        
        endpointController.execute(cryptoRequest, completion: completion)
    }
}
