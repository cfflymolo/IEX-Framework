//
//  EndpointError.swift
//  IEX
//
//  Created by Robert Colin on 3/7/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

public enum EndpointError: Error {
    case invalidJSONResponse(forExpectedType: Any.Type)
    case invalidUrl(host: String, endpoint: String, parameters: [String: Any]?)
    case missingJSONKey(String, forType: Any.Type)
    case unknownError
}

extension EndpointError: CustomStringConvertible {
    public var description: String {
        switch self {
        case let .invalidJSONResponse(type):
            return "Invalid JSON response for type: \(type)."
        case let .invalidUrl(host, endpoint, parameters):
            var errorString: String = "Invalid Url - host: \(host); endpoint: \(endpoint);"
            let parameterString: String? = parameters?.map { "\($0.0): \($0.1)" }.joined(separator: "\n")
            if let parameterString = parameterString {
                errorString = "\(errorString) parameters: [ \n\(parameterString)\n ]"
            }
            
            return errorString
        case let .missingJSONKey(key, type):
            return "JSON response missing expected value of type: \(type), for key: \(key)."
        case .unknownError:
            return "Unkonwn error."
        }
    }
}
