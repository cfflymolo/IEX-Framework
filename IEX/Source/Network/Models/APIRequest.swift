//
//  APIRequest.swift
//  IEX
//
//  Created by Robert Colin on 6/23/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

internal protocol APIRequest {
    var path: String { get }
    var method: HTTPMethod { get }
    var bodyData: Data? { get }
    
    func headers() -> [String: String]?
    func queryParameters() -> [String: String]?
    func parameters() throws -> [String: Any]
}

extension APIRequest where Self: Encodable {
    func parameters() throws -> JSONDictionary {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(
            with: data,
            options: []) as? JSONDictionary
        else {
            throw NSError()
        }
        
        return dictionary
    }
}
