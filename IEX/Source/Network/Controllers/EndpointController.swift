//
//  EndpointController.swift
//  IEX
//
//  Created by Robert Colin on 3/4/18.
//  Copyright © 2018 Robert Colin. All rights reserved.
//

import Foundation

typealias JSONDictionary = [String: Any]

public enum HTTPMethod: String, CustomStringConvertible {
    case delete
    case get
    case head
    case options
    case patch
    case post
    case put
    
    public var description: String {
        return rawValue.uppercased()
    }
}

internal final class EndpointController {
    internal typealias ResultHandler<T> = ((Result<T>) -> ())
    
    // MARK: - Properties
    
    fileprivate let hostUrlString: String
    fileprivate let queue: OperationQueue
    fileprivate let session: URLSession
    
    // MARK: - Initialization
    
    init(hostUrlString: String, queue: OperationQueue, session: URLSession) {
        self.hostUrlString = hostUrlString
        self.queue = queue
        self.session = session
    }
    
    convenience init(hostUrlString: String) {
        let queue: OperationQueue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        
        self.init(hostUrlString: hostUrlString, queue: queue, session: .shared)
    }
    
    // MARK: - Internal
    
    internal func execute<T: Decodable>(
        _ apiRequest: APIRequest,
        completion: @escaping ResultHandler<T>
    )
    {
        guard let urlRequest = urlRequest(
            forPath: apiRequest.path,
            method: apiRequest.method,
            headers: apiRequest.headers(),
            queryParameters: apiRequest.queryParameters(),
            httpBody: apiRequest.bodyData
        )
        else {
            completion(.failure(
                EndpointError.invalidUrl(
                    host: hostUrlString,
                    endpoint: apiRequest.path,
                    parameters: apiRequest.queryParameters()
                )
            ))
            
            return
        }
        
        let networkTask = session.dataTask(with: urlRequest) { data, response, error in
            if let error = error {
                OperationQueue.main.addOperation {
                    completion(.failure(error))
                }
                
                return
            }
            
            guard
                let data = data,
                let decoded = try? JSONDecoder().decode(T.self, from: data)
            else {
                OperationQueue.main.addOperation {
                    completion(.failure(
                        EndpointError.invalidJSONResponse(forExpectedType: T.self)
                    ))
                }
                
                return
            }
            
            OperationQueue.main.addOperation {
                completion(.success(decoded))
            }
        }
        
        queue.addOperation(networkTask.resume)
    }
    
    // MARK: - File Private
    
    fileprivate func urlComponents(
        forPath path: String,
        parameters: [String: String]? = nil
    ) -> URLComponents
    {
        var components: URLComponents = URLComponents()
        components.scheme = "https"
        components.host = hostUrlString
        components.path = path
        components.queryItems = parameters?.map { URLQueryItem(name: $0.key, value: $0.value) }
        
        return components
    }
    
    fileprivate func urlRequest(
        forPath path: String,
        method: HTTPMethod,
        headers: [String: String]? = nil,
        queryParameters: [String: String]? = nil,
        httpBody body: Data? = nil
    ) -> URLRequest?
    {
        guard let url = urlComponents(forPath: path, parameters: queryParameters).url else {
            return nil
        }
        
        var request: URLRequest = URLRequest(url: url)
        request.httpMethod = method.description
        request.httpBody = body
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        headers?.forEach { key, value in request.addValue(value, forHTTPHeaderField: key) }
        
        return request
    }
}
